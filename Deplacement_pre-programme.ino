/*Programme pour faire déplacer le Lowbot*/

/*Définition des borches*/  
  #define enA 5 //enA du L298 connecté à la broche 5 de l'arduino
  #define in1 6 //in1 du L298 connecté à la broche 6 de l'arduino
  #define in2 7 //in2 du L298 connecté à la broche 7 de l'arduino
  #define in3 8 //in3 du L298 connecté à la broche 8 de l'arduino
  #define in4 9 //in4 du L298 connecté à la broche 9 de l'arduino
  #define enB 10  //enB du L298 connecté à la broche 10 de l'arduino

void setup() 
{
  pinMode(enA, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
  pinMode(enB, OUTPUT);

  //Delai avant début
  digitalWrite(in1, LOW);   // in1 et in2 définissent le sens du moteur gauche
  digitalWrite(in2, HIGH);  // in1 en LOW et in2 en HIGH signifient "en avant" MAIS il faut tester car cela dépend du sens de montage du câblage !
  digitalWrite(in3, LOW);   // in3 et in4 définissent le sens du moteur droit
  digitalWrite(in4, HIGH);  // in3 en LOW et in4 en HIGH signifient "en avant" MAIS il faut tester car cela dépend du sens de montage du câblage !
  analogWrite(enA, 0);      // enA suivit du nombre donne la puissance du moteur gauche entre 0 (minimum) et 255 (maximum) Il faut tester différentes valeurs pour que le robot avance droit !!
  analogWrite(enB, 0);      // enB suivit du nombre donne la puissance du moteur droit entre 0 (minimum) et 255 (maximum)
  delay(1000);              // définit la durée de l'action en millisecondes soit 3500 = 3,5 sec
}

void loop()
{
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  analogWrite(enA, 130);
  analogWrite(enB, 150);
  delay(2000);

  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  analogWrite(enA, 0);
  analogWrite(enB, 0);
  delay(500);
  
  // On souhaite avancer le robot pendant 5 secondes
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3,HIGH);
  digitalWrite(in4,LOW);
  analogWrite(enA, 150);
  analogWrite(enB, 150);
  delay(600);

  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  analogWrite(enA, 0);
  analogWrite(enB, 0);
  delay(500);

  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  analogWrite(enA, 130);
  analogWrite(enB, 150);
  delay(2000);

  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  analogWrite(enA, 0);
  analogWrite(enB, 0);
  delay(500);

  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3,HIGH);
  digitalWrite(in4,LOW);
  analogWrite(enA, 150);
  analogWrite(enB, 150);
  delay(600);

  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  analogWrite(enA, 0);
  analogWrite(enB, 0);
  delay(500);
}