/*Définition des broches comme pour le programme déplacement*/  
  #define captD 2 //broche du capteur de droite sous le robot
  #define captG 3 //broche du capteur de gauche sous le robot
  #define enA 5
  #define in1 6
  #define in2 7
  #define in3 8
  #define in4 9
  #define enB 10
 
  int valcaptG;
  int valcaptD;


void setup()
{

  pinMode(enA, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  analogWrite(enA, 0);  //Toujours valeurs à tester pour que le robot avance droit
  analogWrite(enB, 0);  //Toujours valeurs à tester pour que le robot avance droit
  pinMode(captD, INPUT);
  pinMode(captG, INPUT);
  delay(1000);
}

void loop() {

  valcaptD = digitalRead(captD);
  valcaptG = digitalRead(captG);

  if (valcaptG == 0 && valcaptD == 0) {
    digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  analogWrite(enA, 80);
  analogWrite(enB, 80);
  }
  else if (valcaptG == 1 && valcaptD == 0) {
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH);
  analogWrite(enA, 80);
  analogWrite(enB, 80);
  }
  else if (valcaptG == 0 && valcaptD == 1) {
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3,HIGH);
  digitalWrite(in4,LOW);
  analogWrite(enA, 80);
  analogWrite(enB, 80);
  }
  else {
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  analogWrite(enA, 0);
  analogWrite(enB, 0);
  }
}